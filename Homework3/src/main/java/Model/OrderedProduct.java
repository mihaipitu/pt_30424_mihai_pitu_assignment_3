package Model;

public class OrderedProduct {
	private int orderID;
	private int productID;
	private int quantity;
	
	public OrderedProduct(int orderID,int productID,int quantity)
	{
		this.setOrderID(orderID);
		this.setProductID(productID);
		this.setQuantity(quantity);
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
