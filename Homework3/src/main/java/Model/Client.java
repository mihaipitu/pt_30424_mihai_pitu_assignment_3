package Model;

public class Client {
	private int clientID;
	private String name;
	private String address;
	private String eMail;
	private String password;
	
	public Client(int clientID,String name,String address,String eMail,String password)
	{
		this.setClientID(clientID);
		this.setName(name);
		this.setAddress(address);
		this.seteMail(eMail);
		this.setPassword(password);
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String toString()
	{
		String s = null;
		s = " Name: " + getName() + "\n Address: " + getAddress() + "\n E-Mail: " + geteMail();
		return s;
	}

}
