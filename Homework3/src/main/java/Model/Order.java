package Model;

public class Order {
	private int orderID;
	private int clientID;
	
	public Order(int orderID,int clientID){
		this.setOrderID(orderID);
		this.setClientID(clientID);
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
}
