package BLL;

import Model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import BusinessLogicLayer.Validator.ProductPriceValidator;
import BusinessLogicLayer.Validator.ProductQuantityValidator;
import BusinessLogicLayer.Validator.Validator;
import DataAccessLayer.ProductAccessOperations;


public class ProductBLL {
	private List<Validator<Product>> validators;
	
	public ProductBLL()
	{
		validators = new ArrayList<Validator<Product>>();
		validators.add(new ProductPriceValidator());
		validators.add(new ProductQuantityValidator());
	}
	
	public Product findById(int ID)
	{
		Product product = ProductAccessOperations.findByID(ID);
		return product;
	}
	
	public ArrayList<Product> selectAll()
	{
		ArrayList<Product> products = ProductAccessOperations.selectAll();
		return products;
	}
	
	public int insertProduct(Product product)
	{
		for(Validator<Product> v:validators)
		{
			v.validate(product);
		}
		return ProductAccessOperations.insert(product);
	}
	
	public void edit(Product product,String name,int quantity,int price,String description)
	{
		Product prod = ProductAccessOperations.findByID(product.getId());
		if(prod == null)
			throw new NoSuchElementException("The product with id " + product.getId() + " does not exist.");
		ProductAccessOperations.edit(product, name, quantity, price, description);
	}
	
	public void delete(Product product)
	{
		Product prod = ProductAccessOperations.findByID(product.getId());
		if(prod == null)
			throw new NoSuchElementException("The product with id " + product.getId() + " does not exist.");
		ProductAccessOperations.delete(product);
	}
	
	public boolean isNumber(String s)
	{
		for(int i=0;i<s.length();i++)
		{
			char c = s.charAt(i);
			if(!Character.isDigit(c))
				return false;
		}
		return true;
	}
}
