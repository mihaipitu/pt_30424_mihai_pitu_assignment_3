package BLL;

import DataAccessLayer.ClientAccessOperations;
import Model.Client;
import BusinessLogicLayer.Validator.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import BusinessLogicLayer.Validator.ClientEMailValidator;
import BusinessLogicLayer.Validator.ClientPasswordValidator;



public class ClientBLL {
	private List<Validator<Client>> validators;
	
	public ClientBLL()
	{
		validators = new ArrayList<Validator<Client>>();
		validators.add(new ClientEMailValidator());
		validators.add(new ClientPasswordValidator());
	}
	public ArrayList<Client> findAll()
	{
		ArrayList<Client> clients = ClientAccessOperations.findAll();
		if(clients == null)
			System.out.println("There are no clients");
		return clients;
	}
	public Client findClientByID(int clientID)
	{
		Client client = ClientAccessOperations.findByID(clientID);
		if(client == null)
			throw new NoSuchElementException("The client with ID " + clientID + " is not found.");
		return client;
	}
	
	public Client findClientByUserAndPassword(String user,String pass)
	{
		Client client = ClientAccessOperations.findByUserAndPassword(user, pass);
		if(client == null)
			System.out.println("The client with user " + user + " and password " + pass +" is not found.");
		return client;	
	}
	
	public int findMaximumID()
	{
		int ID = ClientAccessOperations.findMaximumID();
		if(ID == 0)
			System.out.println("There are no clients.");
		return ID;
		
	}
	
	public int insertClient(Client client)
	{
		for(Validator<Client> v: validators)
		{
			v.validate(client);
		}
		return ClientAccessOperations.insert(client);
	}
	
	public void deleteClient(Client client)
	{
		Client cl = ClientAccessOperations.findByID(client.getClientID());
		if (!client.equals(cl))
			throw new NoSuchElementException("The client with id " + client.getClientID() + " does not exist.");
		ClientAccessOperations.delete(client);		
	}
	
	public void updateClient(Client client,String name,String address,String eMail,String password)
	{
		Client cl = ClientAccessOperations.findByID(client.getClientID());
		if (!client.equals(cl))
			throw new NoSuchElementException("The client with id " + client.getClientID() + " does not exist.");
		ClientAccessOperations.edit(client, name, address, eMail, password);		
	}
}

