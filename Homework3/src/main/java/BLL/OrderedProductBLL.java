package BLL;

import Model.OrderedProduct;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import BusinessLogicLayer.Validator.OrderedProductQuantityValidator;
import BusinessLogicLayer.Validator.Validator;
import DataAccessLayer.OrderedProductAccessOperations;

public class OrderedProductBLL {
	private List<Validator<OrderedProduct>> validators;
	
	public OrderedProductBLL()
	{
		validators = new ArrayList<Validator<OrderedProduct>>();
		validators.add(new OrderedProductQuantityValidator());
	}
	
	public ArrayList<OrderedProduct> findByID(int orderID)
	{
		ArrayList<OrderedProduct> orderedProduct = OrderedProductAccessOperations.findByID(orderID);
		if(orderedProduct == null)
			throw new NoSuchElementException("The ordered product with orderID " + orderID + " is not found.");
		return orderedProduct;
	}
	
	public int insertProduct(OrderedProduct orderedProduct)
	{
		for(Validator<OrderedProduct> v: validators)
		{
			v.validate(orderedProduct);
		}
		return OrderedProductAccessOperations.insert(orderedProduct);
	}
	
	public void deleteProduct(OrderedProduct orderedProduct)
	{
		OrderedProductAccessOperations.delete(orderedProduct);
	}
	
	public void editProduct(OrderedProduct orderedProduct,int quantity)
	{
		OrderedProductAccessOperations.edit(orderedProduct, quantity);
	}
}
