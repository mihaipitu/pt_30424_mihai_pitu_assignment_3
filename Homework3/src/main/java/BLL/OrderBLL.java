package BLL;

import Model.Order;

import java.util.NoSuchElementException;

import DataAccessLayer.OrderAccessOperations;

	
public class OrderBLL {
	
	public Order findByID(int orderID)
	{
		Order order = OrderAccessOperations.findByID(orderID);
		if(order == null)
			throw new NoSuchElementException("The order with ID " + orderID + " is not found.");
		return order;
	}
	
	public int insert(Order order)
	{
		return OrderAccessOperations.insert(order);
	}
	
	public void delete(Order order)
	{
		Order order2 = OrderAccessOperations.findByID(order.getOrderID());
		if(order2 == null)
			throw new NoSuchElementException("The order with ID " + order.getOrderID() + " is not found.");
		OrderAccessOperations.delete(order);
	}
	
	public int findMaximumID()
	{
		return OrderAccessOperations.findMaximumID();
	}
	
}
