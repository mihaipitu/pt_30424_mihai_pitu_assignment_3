package DataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Model.OrderedProduct;

public class OrderedProductAccessOperations {
	private static final String insertStatementString =  "INSERT INTO orderproducts (OrderID,ID,Quantity)" + " VALUES (?,?,?)";
	private static final String findStatementString = "SELECT * FROM orderproducts WHERE OrderID = ?";
	private static final String deleteStatementString = "DELETE FROM orderproducts WHERE ID = ?";
	private static final String updateStatementString = "UPDATE orderproducts SET Quantity = ? WHERE ID = ? AND OrderID = ?";
	
	public static ArrayList<OrderedProduct> findByID(int orderID)
	{
		ArrayList<OrderedProduct> orders = new ArrayList<OrderedProduct>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		try
		{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, orderID);
			result = findStatement.executeQuery();
			while(result.next())
			{
				int oID = result.getInt("OrderID");
				int pID = result.getInt("ID");
				int quantity = result.getInt("Quantity");
				OrderedProduct order = new OrderedProduct(oID,pID,quantity);
				orders.add(order);
			}
		} catch(SQLException e){
			System.out.println("Warning! Wrong orderID! Try again!");
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return orders;
	}
	
	public static int insert(OrderedProduct order)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		int insertID = -1;
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getOrderID());
			insertStatement.setInt(2, order.getProductID());
			insertStatement.setInt(3, order.getQuantity());
			insertStatement.executeUpdate();
			
			ResultSet result = insertStatement.getGeneratedKeys();
			if(result.next())
			{
				insertID = result.getInt(1);
			}
		} catch(SQLException e){
			System.out.println("Error! Could not insert ordered product in database! Try again!");
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertID;
	}
	
	public static void edit(OrderedProduct orderedProduct,int quantity)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try
		{
			updateStatement = dbConnection.prepareStatement(updateStatementString,Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, quantity);
			updateStatement.setInt(2, orderedProduct.getProductID());
			updateStatement.setInt(3, orderedProduct.getOrderID());
			updateStatement.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static void delete(OrderedProduct orderedProduct)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try
		{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString,Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, orderedProduct.getProductID());
			deleteStatement.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}
