package DataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Model.Client;

public class ClientAccessOperations {
	private static final String insertStatementString =  "INSERT INTO clients (ClientID,Name,Address,EMailAddress,Password)" + " VALUES (?,?,?,?,?)";
	private static final String findStatementStringByID = "SELECT * FROM clients where ClientID = ?";
	private static final String findStatementStringByUserAndPass = "SELECT * FROM clients WHERE EMailAddress = " + "?" + " AND Password = "+ "?";
	private static final String deleteStatementString = "DELETE FROM clients WHERE ClientID = ?";
	private static final String updateStatementString = "UPDATE client SET Name = ? , Address = ? , EMailAddress = ? , Password = ? WHERE ClientID = ?";
	private static final String maxIDStatementString = "SELECT max(ClientID) FROM clients";
	private static final String findStatementString = "SELECT * FROM clients";
	public static ArrayList<Client> findAll()
	{
		ArrayList<Client> clients = new ArrayList<Client>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		try
		{
			findStatement = dbConnection.prepareStatement(findStatementString);
			result = findStatement.executeQuery();
			while(result.next())
			{
				int cID = result.getInt("ClientID");
				String name = result.getString("Name");
				String address = result.getString("Address");
				String eMail = result.getString("EMailAddress");
				String password = result.getString("Password");
				Client client = new Client(cID,name,address,eMail,password);
				clients.add(client);
			}
		} catch(SQLException e){
			System.out.println("Warning! No clients here!");
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return clients;
	} 
	public static Client findByID(int clientID)
	{
		Client client = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		try
		{
			findStatement = dbConnection.prepareStatement(findStatementStringByID);
			findStatement.setLong(1, clientID);
			result = findStatement.executeQuery();
			result.next();
			
			int cID = result.getInt("ClientID");
			String name = result.getString("Name");
			String address = result.getString("Address");
			String eMail = result.getString("EMailAddress");
			String password = result.getString("Password");
			client = new Client(cID,name,address,eMail,password);
		} catch(SQLException e){
			System.out.println("Warning! Wrong clientID! Try again!");
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return client;
	}
	public static int findMaximumID()
	{
		int ID = 0;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		try
		{
			findStatement = dbConnection.prepareStatement(maxIDStatementString);
			result = findStatement.executeQuery();
			result.next();
			ID = result.getInt(1);
		} catch(SQLException e){
			System.out.println("Warning! There are no clients!");
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return ID;
	}
	public static Client findByUserAndPassword(String eMail,String password)
	{
		Client client = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		try
		{
			findStatement = dbConnection.prepareStatement(findStatementStringByUserAndPass);
			findStatement.setString(1, eMail);
			findStatement.setString(2, password);
			result = findStatement.executeQuery();
			if(result.next())
			{
				int cID = result.getInt("ClientID");
				String name = result.getString("Name");
				String address = result.getString("Address");
				String eMails = result.getString("EMailAddress");
				String passwords = result.getString("Password");
				client = new Client(cID,name,address,eMails,passwords);
			}
		} catch(SQLException e){
			System.out.println("Warning! Wrong username and password!");
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return client;
	}
	public static void edit(Client client,String name,String address,String eMail,String password)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try
		{
			updateStatement = dbConnection.prepareStatement(updateStatementString,Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, name);
			updateStatement.setString(2, address);
			updateStatement.setString(3, eMail);
			updateStatement.setString(4, password);
			updateStatement.setInt(5, client.getClientID());
			updateStatement.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static int insert(Client client)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		int insertID = -1;
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, client.getClientID());
			insertStatement.setString(2, client.getName());
			insertStatement.setString(3, client.getAddress());
			insertStatement.setString(4, client.geteMail());
			insertStatement.setString(5, client.getPassword());
			insertStatement.executeUpdate();
			
			ResultSet result = insertStatement.getGeneratedKeys();
			if(result.next())
			{
				insertID = result.getInt(1);
			}
		} catch(SQLException e){
			System.out.println("Error! Could not insert client in database! Try again!");
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertID;
	}
	public static void delete(Client client)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try
		{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString,Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, client.getClientID());
			deleteStatement.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}
