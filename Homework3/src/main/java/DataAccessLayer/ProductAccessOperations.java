package DataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Model.Product;

public class ProductAccessOperations {
	private static final String insertStatementString =  "INSERT INTO products (ID,Name,Quantity,Price,Description)" + " VALUES (?,?,?,?,?)";
	private static final String findStatementString = "SELECT * FROM products WHERE ID = ?";
	private static final String selectStatementString= "SELECT * FROM products";
	private static final String deleteStatementString = "DELETE FROM products WHERE ID = ?";
	private static final String updateStatementString = "UPDATE products SET Name = ?,Quantity = ?,Price = ?,Description = ? WHERE ID = ?";
	
	public static Product findByID(int id)
	{
		Product product = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		try
		{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1,id);
			result = findStatement.executeQuery();
			result.next();
			
			int ID = result.getInt("ID");
			String name = result.getString("Name");
			int quantity = result.getInt("Quantity");
			int price = result.getInt("Price");
			String description = result.getString("Description");
			product = new Product(ID,name,quantity,price,description);
		} catch(SQLException e){
			System.out.println("Warning! Wrong clientID! Try again!");
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return product;
	}
	
	public static ArrayList<Product> selectAll()
	{
		ArrayList<Product> products = new ArrayList<Product>();
		Product product = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		try
		{
			findStatement = dbConnection.prepareStatement(selectStatementString);
			result = findStatement.executeQuery();
			while(result.next())
			{
				int ID = result.getInt("ID");
				String name = result.getString("Name");
				int quantity = result.getInt("Quantity");
				int price = result.getInt("Price");
				String description = result.getString("Description");
				product = new Product(ID,name,quantity,price,description);
				products.add(product);
			}
		} catch(SQLException e){
			System.out.println("Warning! There are no products!");
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return products;
	}
	public static int insert(Product product)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		int insertID = -1;
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, product.getId());
			insertStatement.setString(2, product.getName());
			insertStatement.setInt(3, product.getQuantity());
			insertStatement.setInt(4, product.getPrice());
			insertStatement.setString(5, product.getDescription());
			insertStatement.executeUpdate();
			
			ResultSet result = insertStatement.getGeneratedKeys();
			if(result.next())
			{
				insertID = result.getInt(1);
			}
		} catch(SQLException e){
			System.out.println("Error! Could not insert client in database! Try again!");
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertID;
	}
	
	public static void edit(Product product,String name,int quantity,int price,String description)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try
		{
			updateStatement = dbConnection.prepareStatement(updateStatementString,Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, name);
			updateStatement.setInt(2, quantity);
			updateStatement.setInt(3, price);
			updateStatement.setString(4, description);
			updateStatement.setInt(5, product.getId());
			updateStatement.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void delete(Product product)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try
		{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString,Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, product.getId());
			deleteStatement.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}
