package DataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Model.Order;

public class OrderAccessOperations {
	private static final String insertStatementString =  "INSERT INTO orders (OrderID,ClientID)" + " VALUES (?,?)";
	private static final String findStatementString = "SELECT * FROM orders WHERE OrderID = ?";
	private static final String deleteStatementString = "DELETE FROM orders WHERE OrderID = ?";
	private static final String maxIDStatementString = "SELECT max(OrderID) FROM orders";
	
	public static Order findByID(int orderID)
	{
		Order order = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		try
		{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, orderID);
			result = findStatement.executeQuery();
			result.next();
			
			int oID = result.getInt("OrderID");
			int cID = result.getInt("ClientID");
			order = new Order(oID,cID);
		
		} catch(SQLException e){
			System.out.println("Warning! Wrong orderID! Try again!");
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return order;
	}
	
	public static int findMaximumID()
	{
		int ID = 0;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		try
		{
			findStatement = dbConnection.prepareStatement(maxIDStatementString);
			result = findStatement.executeQuery();
			result.next();
			ID = result.getInt(1);
		} catch(SQLException e){
			System.out.println("Warning! There are no orders!");
		} finally {
			ConnectionFactory.close(result);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return ID;
	}
	
	public static int insert(Order order)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		int insertID = -1;
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getOrderID());
			insertStatement.setInt(2, order.getClientID());
			insertStatement.executeUpdate();
			
			ResultSet result = insertStatement.getGeneratedKeys();
			if(result.next())
			{
				insertID = result.getInt(1);
			}
		} catch(SQLException e){
			System.out.println("Error! Could not insert order in database! Try again!");
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertID;
	}
	
	public static void delete(Order order)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try
		{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString,Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, order.getOrderID());
			deleteStatement.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}
