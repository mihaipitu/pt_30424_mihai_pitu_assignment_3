package BusinessLogicLayer.Validator;

import Model.Product;

public class ProductPriceValidator implements Validator<Product>{
	
	public boolean validate(Product product)
	{
		if(product.getPrice()<=0)
			return false;
		return true;
	}

}
