package BusinessLogicLayer.Validator;

import Model.Client;
import java.util.regex.Pattern;

public class ClientEMailValidator implements Validator<Client>{
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public boolean validate(Client client)
	{
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		return pattern.matcher(client.geteMail()).matches();
	}
}
