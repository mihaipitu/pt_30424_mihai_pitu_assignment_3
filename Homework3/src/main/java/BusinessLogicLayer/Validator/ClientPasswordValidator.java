package BusinessLogicLayer.Validator;

import Model.Client;

public class ClientPasswordValidator implements Validator<Client>{
	
	private int MIN_LENGTH = 4;
	private int MAX_LENGTH = 12;
	
	public boolean validate(Client client)
	{
		int length = client.getPassword().length();
		if(length < MIN_LENGTH || length > MAX_LENGTH)
			return false;
		return true;
	}

}
