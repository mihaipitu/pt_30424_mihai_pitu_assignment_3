package BusinessLogicLayer.Validator;

import Model.OrderedProduct;

public class OrderedProductQuantityValidator implements Validator<OrderedProduct>{
	
	public boolean validate(OrderedProduct orderedProduct)
	{
		if(orderedProduct.getQuantity()<=0)
			return false;
		return true;
	}

}
