package BusinessLogicLayer.Validator;

public interface Validator<T> {

	public boolean validate(T t);
}
