package BusinessLogicLayer.Validator;

import Model.Product;

public class ProductQuantityValidator implements Validator<Product>{

	public boolean validate(Product product)
	{
		if(product.getQuantity()<0)
			return false;
		return true;
	}
}
