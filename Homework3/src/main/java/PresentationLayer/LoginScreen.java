package PresentationLayer;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Model.Client;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Window;

import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import BLL.ClientBLL;
public class LoginScreen extends JFrame {

	private JPanel loginPanel;
	private JTextField eMailField;
	private JPasswordField passwordField;
	private ClientBLL clientOperation = new ClientBLL();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginScreen frame = new LoginScreen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginScreen() {
		setTitle("Warehouse Shop");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 480, 280);
		loginPanel = new JPanel();
		loginPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(loginPanel);
		loginPanel.setLayout(null);
		
		JLabel lblWelcomeToThe = new JLabel("Welcome to the shop!");
		lblWelcomeToThe.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcomeToThe.setFont(new Font("Times New Roman", Font.BOLD, 24));
		lblWelcomeToThe.setBounds(76, 11, 224, 63);
		loginPanel.add(lblWelcomeToThe);
		
		eMailField = new JTextField();
		eMailField.setBounds(76, 112, 86, 20);
		loginPanel.add(eMailField);
		eMailField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(76, 143, 86, 20);
		loginPanel.add(passwordField);
		
		JLabel lblUser = new JLabel("User");
		lblUser.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUser.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblUser.setBounds(20, 114, 46, 14);
		loginPanel.add(lblUser);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setBounds(3, 145, 63, 14);
		loginPanel.add(lblPassword);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = eMailField.getText();
				@SuppressWarnings("deprecation")
				String password = passwordField.getText();
				Client client = clientOperation.findClientByUserAndPassword(username, password);
				if(client == null)
					JOptionPane.showMessageDialog(null, "Error! Can't login!");
				else
				{	
					MainMenu mainMenu = new MainMenu(client);
					dispose();
					mainMenu.setVisible(true);
				}
			}
		});
		btnLogin.setBounds(73, 174, 89, 23);
		loginPanel.add(btnLogin);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogin.setBounds(20, 73, 142, 30);
		loginPanel.add(lblLogin);
		
		JLabel lblNewClient = new JLabel("New client?");
		lblNewClient.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNewClient.setBounds(276, 83, 99, 20);
		loginPanel.add(lblNewClient);
		
		JLabel lblPressHereTo = new JLabel("Press here to register");
		lblPressHereTo.setHorizontalAlignment(SwingConstants.CENTER);
		lblPressHereTo.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblPressHereTo.setBounds(244, 112, 172, 20);
		loginPanel.add(lblPressHereTo);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegisterClient registerClient = new RegisterClient();
				dispose();
				registerClient.setVisible(true);
			}
		});
		btnRegister.setBounds(286, 142, 89, 23);
		loginPanel.add(btnRegister);
	}
}
