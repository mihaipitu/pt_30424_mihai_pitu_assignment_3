package PresentationLayer;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import BLL.OrderBLL;
import BLL.OrderedProductBLL;
import BLL.ProductBLL;
import Model.OrderedProduct;
import Model.Product;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InsertOrderedProducts extends JFrame {

	private JPanel contentPane;
	private JTextField productIDField;
	private JTextField quantityField;
    private ProductBLL productOperations = new ProductBLL();
    private OrderedProductBLL orderedProductOperations = new OrderedProductBLL();
    private OrderBLL orderOperations = new OrderBLL();
    private Product product;
    private OrderedProduct orderedProduct;
    private int orderID;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InsertOrderedProducts frame = new InsertOrderedProducts();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InsertOrderedProducts() {
		setTitle("Order products");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 352, 289);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		productIDField = new JTextField();
		productIDField.setBounds(95, 80, 86, 20);
		contentPane.add(productIDField);
		productIDField.setColumns(10);
		
		JLabel lblProductId = new JLabel("Product ID");
		lblProductId.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblProductId.setHorizontalAlignment(SwingConstants.RIGHT);
		lblProductId.setBounds(10, 79, 75, 20);
		contentPane.add(lblProductId);
		
		quantityField = new JTextField();
		quantityField.setBounds(95, 111, 86, 20);
		contentPane.add(quantityField);
		quantityField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Quantity");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 113, 65, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnInsertProduct = new JButton("Insert product");
		btnInsertProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String productIDString = productIDField.getText();
				String quantityString = quantityField.getText();
				
				if(!productOperations.isNumber(productIDString))
				{
					JOptionPane.showMessageDialog(null, "Insert a valid productID (an integer)!");
				}
				else
					if(!productOperations.isNumber(quantityString))
					{
						JOptionPane.showMessageDialog(null, "Insert a valid quantity (an integer)!");
					}
					else
					{
						int productID = Integer.parseInt(productIDString);
						int quantity = Integer.parseInt(quantityString);
						if(quantity <= 0)
						{
							JOptionPane.showMessageDialog(null, "Insert a valid quantity (greater than 0)!");
						}
						else							
						{
							product = productOperations.findById(productID);
							if(product == null)
								JOptionPane.showMessageDialog(null, "There is no product with the ID " + productID + "! Try again!");
							else
								if(product.getQuantity() < quantity)
									JOptionPane.showMessageDialog(null, "Warning! The quantity you want to order is too much than in the stock! Try again!");
								else
								{
									JOptionPane.showMessageDialog(null, "Product with the ID: " + productID +" has been inserted into your order!");
									orderID = orderOperations.findMaximumID();
									orderedProduct = new OrderedProduct(orderID,productID,quantity);
									orderedProductOperations.insertProduct(orderedProduct);
									productOperations.edit(product, product.getName(), product.getQuantity()-quantity, product.getPrice(), product.getDescription());								
								}
						}
					}	
			}
		});
		btnInsertProduct.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnInsertProduct.setBounds(191, 80, 145, 51);
		contentPane.add(btnInsertProduct);
		
		JButton btnNewButton = new JButton("Finish order");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,"Order finished. Press `View Bill of Order` to see your bill!");
				dispose();
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnNewButton.setBounds(95, 164, 145, 75);
		contentPane.add(btnNewButton);
		
		JLabel lblInsertYourDesired = new JLabel("Insert your desired products in your order");
		lblInsertYourDesired.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblInsertYourDesired.setHorizontalAlignment(SwingConstants.CENTER);
		lblInsertYourDesired.setBounds(10, 34, 326, 34);
		contentPane.add(lblInsertYourDesired);
	}
}
