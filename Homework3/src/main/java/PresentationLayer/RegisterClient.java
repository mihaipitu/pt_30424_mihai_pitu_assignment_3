package PresentationLayer;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import BLL.ClientBLL;
import Model.Client;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegisterClient extends JFrame {

	private JPanel contentPane;
	private JTextField nameField;
	private JTextField addressField;
	private JTextField eMailField;
	private JPasswordField passwordField;
	private ClientBLL clientOperations = new ClientBLL();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterClient frame = new RegisterClient();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegisterClient() {
		setTitle("Register");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblInsertYourData = new JLabel("Insert your data here");
		lblInsertYourData.setHorizontalAlignment(SwingConstants.CENTER);
		lblInsertYourData.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblInsertYourData.setBounds(126, 11, 184, 24);
		contentPane.add(lblInsertYourData);
		
		nameField = new JTextField();
		nameField.setBounds(168, 46, 86, 20);
		contentPane.add(nameField);
		nameField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(92, 49, 66, 14);
		contentPane.add(lblNewLabel);
		
		addressField = new JTextField();
		addressField.setBounds(168, 77, 86, 20);
		contentPane.add(addressField);
		addressField.setColumns(10);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblAddress.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAddress.setBounds(112, 80, 46, 14);
		contentPane.add(lblAddress);
		
		eMailField = new JTextField();
		eMailField.setBounds(168, 108, 86, 20);
		contentPane.add(eMailField);
		eMailField.setColumns(10);
		
		JLabel lblEmailAddress = new JLabel("E-Mail Address");
		lblEmailAddress.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblEmailAddress.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmailAddress.setBounds(63, 111, 95, 14);
		contentPane.add(lblEmailAddress);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(168, 139, 86, 20);
		contentPane.add(passwordField);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblPassword.setBounds(92, 142, 66, 14);
		contentPane.add(lblPassword);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name =  nameField.getText();
				String address = addressField.getText();
				String eMail = eMailField.getText();
				String password = passwordField.getText();
				int maxID = clientOperations.findMaximumID();
				if(maxID == 0)
					System.out.println("Error! maxID");
				else
				{
					Client client = new Client(maxID+1,name,address,eMail,password);
					clientOperations.insertClient(client);
					JOptionPane.showMessageDialog(null, "Welcome, " + name + " to the shop!");
					MainMenu mainMenu = new MainMenu(client);
					dispose();
					mainMenu.setVisible(true);
				}
			}
		});
		btnRegister.setBounds(168, 170, 89, 23);
		contentPane.add(btnRegister);
	}

}
