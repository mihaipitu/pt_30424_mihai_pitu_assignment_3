package PresentationLayer;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;

import BLL.ClientBLL;
import BLL.OrderBLL;
import BLL.ProductBLL;
import Model.Client;
import Model.Order;
import Model.Product;
import net.proteanit.sql.DbUtils;
import javax.swing.table.DefaultTableModel;

public class MainMenu extends JFrame {

	private JPanel contentPane;
	private JTable mainTable;
	private ClientBLL clientOperations =  new ClientBLL();
	private ProductBLL products = new ProductBLL();
	private OrderBLL orderOperations = new OrderBLL();
	private Order order;
	private Client client;
	private OutputFileText output;
	private JTable clientTable;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client cl = new Client(5,"Robot","Wall","robot","1234");
					MainMenu frame = new MainMenu(cl);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainMenu(final Client client) 
	{
		setClient(client);
		setTitle("Warehouse Shop");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 629, 453);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 11, 372, 187);
		contentPane.add(scrollPane);
		
		mainTable = new JTable();
		mainTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ProductID", "Name", "Quantity", "Price", "Description"
			}
		));
		scrollPane.setViewportView(mainTable);
		
		JButton btnShowProducts = new JButton("Show Products");
		btnShowProducts.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnShowProducts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<Product> prod = products.selectAll();
				DefaultTableModel model = (DefaultTableModel) mainTable.getModel();
				Object[] rows = new Object[5];
				model.setRowCount(0);
				for(int i=0;i<prod.size();i++)
				{
					rows[0] = prod.get(i).getId();
					rows[1] = prod.get(i).getName();
					rows[2] = prod.get(i).getQuantity();
					rows[3] = prod.get(i).getPrice();
					rows[4] = prod.get(i).getDescription();
					model.addRow(rows);
				}	
			}
		});
		btnShowProducts.setBounds(434, 51, 169, 39);
		contentPane.add(btnShowProducts);
		
		JLabel lblWelcome = new JLabel("Welcome, "+ client.getName() +"!");
		lblWelcome.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblWelcome.setBounds(434, 11, 169, 29);
		contentPane.add(lblWelcome);
		
		JButton btnCreateOrder = new JButton("Create a new order");
		btnCreateOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int orderID = orderOperations.findMaximumID();
				order = new Order(orderID+1,client.getClientID());
				orderOperations.insert(order);
				InsertOrderedProducts insertOrder = new InsertOrderedProducts();
				insertOrder.setVisible(true);
			}
		});
		btnCreateOrder.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnCreateOrder.setBounds(434, 101, 169, 39);
		contentPane.add(btnCreateOrder);
		
		JButton btnViewFinalOrder = new JButton("View Bill of Order");
		btnViewFinalOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Order order = new Order(orderOperations.findMaximumID(),client.getClientID());
				output = new OutputFileText(order);
				try {
					Desktop.getDesktop().open(output.outputFile);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnViewFinalOrder.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnViewFinalOrder.setBounds(434, 151, 169, 39);
		contentPane.add(btnViewFinalOrder);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(21, 225, 372, 178);
		contentPane.add(scrollPane_1);
		
		clientTable = new JTable();
		clientTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Client ID", "name", "address", "eMail"
			}
		));
		scrollPane_1.setViewportView(clientTable);
		
		JButton btnNewButton = new JButton("Show other clients");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<Client> clients = clientOperations.findAll();
				DefaultTableModel model = (DefaultTableModel) clientTable.getModel();
				Object[] rows = new Object[4];
				model.setRowCount(0);
				for(int i=0;i<clients.size();i++)
				{
					rows[0] = clients.get(i).getClientID();
					rows[1] = clients.get(i).getName();
					rows[2] = clients.get(i).getAddress();
					rows[3] = clients.get(i).geteMail();
					model.addRow(rows);
				}
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnNewButton.setBounds(434, 201, 169, 39);
		contentPane.add(btnNewButton);
	}
	
	public void setClient(Client client)
	{
		this.client = client;
	}
}
