package PresentationLayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import BLL.ClientBLL;
import BLL.OrderedProductBLL;
import BLL.ProductBLL;
import Model.Client;
import Model.Order;
import Model.OrderedProduct;
import Model.Product;

import java.io.IOException;
import java.io.PrintWriter;

public class OutputFileText{
	public File outputFile = new File("bill.txt");
	private PrintWriter out;
	private Client client;
	private Order order;
	private List<OrderedProduct> orderProducts;
	private ClientBLL clientOperations = new ClientBLL();
	private OrderedProductBLL orderedProductsOperations = new OrderedProductBLL();
	private ProductBLL productOperations = new ProductBLL();
	
	public OutputFileText(Order order)
	{
		createFile();
		
		try {
			out = new PrintWriter(outputFile);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		orderProducts = new ArrayList<OrderedProduct>();
		this.order = order;
		try {
			writeOrder();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void createFile()
	{
		try
		{
			if(!outputFile.exists())
				outputFile.createNewFile();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void writeOrder() throws IOException
	{
		int sum = 0;
		out.println("This is your final order! You are:\n");
		client = clientOperations.findClientByID(order.getClientID());
		out.println(client.toString());
		out.println();
		out.println("\nYour order no is: " + order.getOrderID()+ "\n\n");
		out.println("You have ordered:\n");
		orderProducts = orderedProductsOperations.findByID(order.getOrderID());
		for(OrderedProduct orderProduct: orderProducts)
		{
			int value;
			Product product = productOperations.findById(orderProduct.getProductID());
			value = orderProduct.getQuantity() * product.getPrice();
			sum+=value;
			out.println(product.getName() + "..." + orderProduct.getQuantity() + "*"+ product.getPrice() + " = " + value + "£\n");
		}
		out.println("\nTotal value of the order: "+sum+"£.\n\nThank you and come again!");
		if(out!= null)
			out.close();
	}
}
